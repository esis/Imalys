## Imalys – Image Analysis

[![Project Status: Active – The project has reached a stable, usable state and is being actively developed.](https://www.repostatus.org/badges/latest/active.svg)](https://www.repostatus.org/#active)

*Imalys* is a software library for landscape analysis based on satellite images. The library provides tools to select, extract, transform and combine raster and vector data. Image quality, landscape diversity, change and different landuse types can be analyzed in time and space. Landuse borders can be delineated and typical landscape structures can be characterized by a self adjusting process.

*Imalys* was designed as a collection of building blocks (tools) to extract defined landscape properties (traits) from public available data. The whole process chain is controlled by predefined hooks and runs without manual interaction. The tools are interchangeable and can be rearranged for new tasks. *Imalys* is available as source code and as executable files. *Imalys* is designed to run under a server environment. 

For detailed information please refer to our [Documentation](https://rdm.pages.ufz.de/imalys-doc/).

___


![Change](img/Change_Bode_Hohes-Holz_Großer-Bruch_2014-21.png)


*Change between spring and autumn calculated as variance during the growing seasons 2014 to 2021. The image is segmented to a seamless mosaic of zones with homogeneous features. Values calculated from Landsat-8 scenes at the Bode catchment (Saxonia, Germany). Value range [0 … 0.5] depicted as blue … red  .* ![](img/Palette_Turbo.png)

___


### Documentation

Here is our online [documentation](https://rdm.pages.ufz.de/imalys-doc/). 

### Changelog

*Imalys* is under development. The most recent version was focused on methods to select and extract appropriate images from large data collections as shipped by the providers and link them to a seamless and high quality product for a freely selectable region. A time series of the whole of Germany using TK-100 sheets with approx. 50,000 image maps [Pangaea](https://pangaea.de/) is an example of this intention. Tools for change detection, outliers and trends will be the next step.

All notable changes to this project will be documented in [CHANGELOG.md](CHANGELOG.md).

### Get involved

*Imalys* is the answer of our current needs to detect landscape differences or changes in space and time using remote sensing data. We faced the need to process large amounts of data with as little manual interaction as possible. Our solutions are predefined process chains that return defined landscape features. We call them "traits". The process chains should be independent from scaling, geographic location and sensor properties as much as possible. The concept of these process chains and their implementation as algorithms is still under development. 

*Imalys* is controlled completely by hooks that assign import data and fix the sequence of the necessary processing steps. Hooks don't require coding expertise. New process chains and new traits can be defined only by hooks. We would like to hear of both, ideas and contribution what can be extracted and how!

### Contributing

If you found a bug or want to suggest some interesting features, please refer to our [contributing guidelines](CONTRIBUTING.md) to see how you can contribute to *Imalys*.

### User support

If you need help or have a question, you can use the Imalys user support: [imalys-support@ufz.de](mailto:imalys-support@ufz.de)

### Copyright and License

Copyright(c) 2023, [Helmholtz-Zentrum für Umweltforschung GmbH -- UFZ](https://www.ufz.de). All rights reserved.

- Documentation: [Creative Commons Attribution 4.0 International](https://creativecommons.org/licenses/by/4.0/) <a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/80x15.png" /></a>

- Source code: [GNU General Public License 3](https://www.gnu.org/licenses/gpl-3.0.html)

For full details, see [LICENSE](LICENSE.md).

### Acknowledgements

### Publications

### How to cite Imalys

If Imalys is advancing your research, please cite as:

> Selsam, Peter; Gey, Ronny; Lausch, Angela; Bumberger, Jan. (2023). Imalys (0.1). Zenodo. <https://doi.org/10.5281/zenodo.8116370>

See also the [CITATION.cff](CITATION.cff).

-----------------
<a href="https://www.ufz.de/index.php?en=33573">
    <img src="https://git.ufz.de/rdm-software/saqc/raw/develop/docs/resources/images/representative/UFZLogo.png" width="400"/>
</a>

<a href="https://www.ufz.de/index.php?en=45348">
    <img src="https://git.ufz.de/rdm-software/saqc/raw/develop/docs/resources/images/representative/RDMLogo.png" align="right" width="220"/>
</a>
