unit thema;

{ THEMA sammelt Routinen zur Clusterung und Klassifikation von Bilddaten. Dabei
  clustert "Model" Bildmerkmale anhand einzelner Pixel und "Fabric" verwendet
  aus der Zellbildung (index) abgeleitete Teilflächen und ihre räumliche
  Verknüpfung. In jedem Fall nimmt "Thema" zuerst Stichproben ("Samples") aus
  den Bilddaten, erzeugt ein "Model" mit verschiedenen Clustern und
  klassifiziert damit das Bild.

  FABRIC: erzeugt und clustert Objekte aus verknüpften Zonen
  LIMITS: extrahiert Masken(Grenzen) aus Werten im Bild
  MODEL:  clustert multidimensionale Bilddaten
  REDUCE: selektiert und analysiert Drei- und mehrdimensionale Bilddaten

  BEGRIFFE MIT SPEZIFISCHER ANWENDUNG:
  Band:    Bildkanal
  Dict:    Zuweisung von Begriffen oder Werten: Bezeichner = Wert
  Feature: Bild-Merkmal (Dichte) in einem →Modell oder einer →Sample-Liste
  Key:     Zonen-Merkmal = Häufigkeit von Kontakten in einem →Model oder einer
           →Sample-Liste
  Layer:   Bildkanal oder Bild-Merkmal
  Model:   Ergebnis einer Clusterung (Selbstorganisation) mit den typischen
           Merkmalen (Werten) der Klassen als Matrix[Klasse,Merkmal].
           Model[?,0] speichert das Quadrat des Suchradius (für SOM-Neurone),
           alle anderen Werte sind Bildmerkmale, auch NoData.
  Samples: Stichproben mit allen Merkmalen eines Pixels oder einer →Zone als
           Matrix[Probe,Merkmal]. Die erste Stelle (Sample[?,0]) bleibt frei
           für den Suchradius (SOM-Neurone).
  Stack:   Stapel aus Kanälen mit Bilddaten, gleiche Geometrie
  Sync:    Imalys kann Merkmale von mehr als einem Bild gemeinsam bearbeiten.
           Dazu müssen die Bilder einen Teil des Namens gemeinsam haben. Lage
           und Geometrie der Bilder sind frei.
  Wave:    zeitlich konstante Periode in der Veränderung von Werten
}

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Math, StrUtils, DateUtils, format;

type
  tFig2Int = function(fVal,fFct:single):string;

  tFabric = class(tObject)
    private
      iacDim: tnInt; //Index auf "iacNbr, iacPrm"
      iacMap: tnInt; //Klassen-Attribut
      iacNbr: tnInt; //Index der Nachbarzelle
      iacPrm: tnInt; //Kontakte zur Nachbarzelle
      function ClassifyKeys(bDbl:boolean; fxKey:tn2Sgl):TnInt;
      function DoubleKeys(iIdx,iMap:integer):TnSgl;
      function KeyModel(bDbl:boolean; iFbr,iSmp:integer):Tn2Sgl;
      function KeySamples(bDbl:boolean; iSmp,iTyp:integer):Tn2Sgl;
      function SingleKeys(iIdx,iMap:integer):TnSgl;
    public
      procedure xFabric(bDbl:boolean; iFbr,iSmp:integer);
  end;

  tLimits = class(tObject)
    private
      function _MaskLimit_(fMin:single; fxBnd:tn2Sgl):tn2Byt;
    public
      procedure _Execute_(fMin:single; sCmd,sImg:string);
  end;

  tModel = class(tObject) //Multi-Trait-Listen clustern und klassifizieren
    const
      ccFct = 3; //Dämpfung bei Werte-Anpassung
      ccScl = 9; //Radius-Scalierung (quadriert)
    private
      procedure Adjust(fxMdl,fxSmp:tn2Sgl; iaThm:tnInt);
      function AttributeSamples(iSmp:integer):Tn2Sgl;
      function FeatureClassify(fxMdl:tn2Sgl):tnInt;
      procedure FeatureModel(fxMdl,fxSmp:tn2Sgl);
      function ImageClassify(fxMdl:tn2Sgl; sImg:string):tn2Byt;
      function ImageSamples(iSmp:integer; sImg:string):Tn2Sgl;
      function IsoSubset(fxSmp:Tn2Sgl; iSub:integer):Tn2Sgl;
      procedure RadiusDefault(fxMdl:tn2Sgl);
      procedure RadiusOpen(fxMdl:tn2Sgl);
      function SampleClassify(fxMdl,fxSmp:Tn2Sgl; iaThm:TnInt):integer;
      function SampleThema(faSmp:TnSgl; fxMdl:tn2Sgl):integer;
      function SpectralMap(iFtr,iSmp:integer; sImg:string):tn2Byt;
      procedure Sync_Classify(sImg:string);
      function Sync_Samples(iSmp:integer; sImg:string):tn2Sgl;
      function ZonalMap(iFbr,iSmp:integer):tnInt;
    public
      procedure xModel(iFtr,iSmp:integer; sImg:string);
      function FeatureDist:tn2Sgl;
      procedure _SyncModel_(iMdl,iSmp:integer; sImg:string);
  end;

  tReduce = class(tObject)
    private
      function BestOf(fxImg:tn3Sgl; sQap:string):tn2Sgl;
      function CommonDate(slImg:tStringList):tStringList;
      function _CoVariance_(fxImg:tn3Sgl; iaTms:tnInt):tn2Sgl;
      function Distance(fxImg:tn3Sgl):tn2Sgl;
      function Execute(fxImg:tn3Sgl; iNir,iRed:integer; sCmd,s_Xpq:string):tn2Sgl;
      function ImageDate(sImg:string):tDateTime;
      function _LeafAreaIndex(fxImg:tn3Sgl; iNir,iRed:integer):tn2Sgl;
      function _LeafAreaIndex_(fxImg:tn3Sgl; iNir,iRed:integer):tn2Sgl;
      function MeanValue(fxImg:tn3Sgl):tn2Sgl;
      function Median(fxImg:tn3Sgl):tn2Sgl;
      function Overlay(fxImg:tn3Sgl):tn2Sgl;
      function _Quality(sQlt:string):single;
      procedure QuickSort(faDev:tnSgl; iDim:integer);
      function Regression(fxImg:tn3Sgl):tn2Sgl;
      function _SentinelQuality_(rFrm:trFrm; sImg,sMsk:string):tn2Byt;
      function Variance(fxImg:tn3Sgl):tn2Sgl;
      function Vegetation(fxImg:tn3Sgl; iNir,iRed,iTyp:integer):tn2Sgl;
    public
      procedure IndexSort(iaIdx:tnInt; faVal:tnSgl);
      function Principal(fxImg:tn3Sgl):tn2Sgl;
      procedure xOverlay(slImg:tStringList);
      procedure xSplice(sCmd,sImg,sTrg:string);
      procedure xReduce(iNir,iRed:integer; sCmd,sImg,sTrg:string);
  end;

var
  Fabric: tFabric;
  Limits: tLimits;
  Model: tModel;
  Reduce: tReduce;

implementation

uses
  index, mutual, raster, vector;

function Float2Number(fVal,fFct:single):string;
begin
  Result:=FloatToStrF(fVal*fFct,ffFixed,7,0)
end;

function Int2Number(fVal,fFct:single):string;
begin
  Result:=FloatToStrF(integer(fVal)*fFct,ffFixed,7,0)
end;

function tReduce._CoVariance_(
  fxImg:tn3Sgl; //Bilddaten-Stack
  iaTms:tnInt): //Zeitstempel pro Bild
  tn2Sgl; //CoVarianz
{ rVc }
{ CoVarianz = (∑xy - ∑x∑y/n)/(n-1) }
var
  fPrd:single=0; //Produkt beider Variablen
  fSum:single=0; //Summe der ersten Variablen
  fMus:single=0; //Summe der zweiten Variablen
  iCnt:integer; //Anzahl gültiger Kanäle (n)
  B,X,Y: integer;
begin
  Result:=Tools.Init2Single(length(fxImg[0]),length(fxImg[0,0]),dWord(NaN)); //Vorgabe = NoData
  for B:=0 to high(fxImg) do //alle Kanäle
    fMus+=iaTms[B]; //Summe aller Zeitstempel
  for Y:=0 to high(fxImg[0]) do
    for X:=0 to high(fxImg[0,0]) do
    begin
      fPrd:=0; fSum:=0; iCnt:=0;
      for B:=0 to high(fxImg) do //alle Kanäle
      begin
        if IsNan(fxImg[B,Y,X]) then continue;
        fPrd:=fxImg[B,Y,X]*iaTms[B];
        fSum+=fxImg[B,Y,X];
        inc(iCnt) //Anzahl gültige Schritte
      end;
      if iCnt>1
        then Result[Y,X]:=(fPrd-fSum*fMus/iCnt)/pred(iCnt)
        else Result[Y,X]:=NaN;
    end;
end;

{ mAS gibt "iSmp" Stichproben aus der Attribut-Tabelle zurück, die zufällig
  über den Zonen-Index verteilt sind. Dazu wählt mAS zufällig einzelne Pixel im
  Index und gibt die Attribute der entsprechenden Zelle zurück. Die Samples
  sind dabei geographisch gleichmäßig verteilt. Große Zonen können mehr als
  einmal getroffen sein. }

function tModel.AttributeSamples(
  iSmp: integer): //Anzahl Stichproben
  Tn2Sgl; //Stichproben[Probe][Merkmale]
const
  cSmp = 'tMAS: Amount of samples must be greater than 1!';
var
  fxAtr: Tn2Sgl=nil; //spektrale Attribute
  ixIdx: Tn2Int=nil; //Zellindex (Zeiger auf fxTmp[0])
  rHdr: trHdr; //Metadaten
  B,S,X,Y: integer;
begin
  Result:=nil;
  if iSmp<2 then Tools.ErrorOut(cSmp);
  fxAtr:=Tools.BitRead(eeHme+cfAtr); //Attribut-Tabelle lesen
  Result:=Tools.Init2Single(iSmp,succ(length(fxAtr)),0); //Merkmale-Liste
  rHdr:=Header.Read(eeHme+cfIdx); //Metadaten
  ixIdx:=tn2Int(Image.ReadBand(0,rHdr,eeHme+cfIdx)); //Zonen-IDs
  RandSeed:=cfRds; //Reihe zurückstellen
  for S:=0 to high(Result) do //alle Beispiele
  begin
    repeat
      Y:=random(length(ixIdx));
      X:=random(length(ixIdx[0]))
    until ixIdx[Y,X]>0; //nur definierte Orte
    for B:=1 to length(fxAtr) do
      Result[S,B]:=fxAtr[pred(B),ixIdx[Y,X]]; //Merkmale
    Result[S,0]:=0 //Vorgabe
  end;
  Header.Clear(rHdr);
  Tools.HintOut('Model.AttributeSamples: '+IntToStr(iSmp));
end;

function tModel.ImageSamples(
  iSmp: integer; //Anzahl Stichproben
  sImg: string): //Vorbild (ENVI-Format)
  Tn2Sgl; //Stichproben[Probe][Merkmale]
{ IS gibt "iSmp" Stichproben aus den Bilddaten zurück, die zufällig über die
  Bildfläche verteilt sind. Dazu wählt IS mit einem Zufalls-Generator einzelne
  Pixel im Bild und gibt die Werte aller Kanäle des Pixels als Array zurück.
  Die Proben sind lineare Arrays. Array[0] nimmt später den Suchradius auf. }
const
  cSmp = 'tMAS: Amount of samples must be greater than 1!';
var
  fxImg: tn3Sgl=nil; //Bilddaten, alle Kanäle
  rHdr: trHdr; //Metadaten
  B,S,X,Y: integer;
begin
  Result:=nil;
  if iSmp<2 then Tools.ErrorOut(cSmp);
  rHdr:=Header.Read(sImg); //Metadaten
  fxImg:=Image.Read(rHdr,sImg); //Bild mit allen Kanälen
  Result:=Tools.Init2Single(iSmp,succ(length(fxImg)),0); //Merkmale-Liste, Suchradius + alle Kanäle
  RandSeed:=cfRds; //Reihe zurückstellen
  for S:=0 to high(Result) do //alle Beispiele
  begin
    repeat
      Y:=random(rHdr.Lin);
      X:=random(rHdr.Scn)
    until not isNaN(fxImg[0,Y,X]); //nur definierte Orte
    for B:=0 to high(fxImg) do
      Result[S,succ(B)]:=fxImg[B,Y,X]; //Dichte-Kombination,
    Result[S,0]:=0; //Vorgabe
  end;
  Header.Clear(rHdr);
  Tools.HintOut('Model.Samples: '+IntToStr(iSmp));
end;

{ IS erzeugt aus der Liste "fxSmp" ein Subset mit "iSub" Stichproben. IS gibt
  Proben zurück, die den Merkmalsraum möglichst gleichmäßig ausfüllen. Dazu läd
  IS in Result[0] iterativ weitere, zufällige Proben, vergleicht die Distanz
  aller anderen Proben untereinander und ersetzt die Probe mit der keinsten
  Distanz durch die neue. IS bricht ab, wenn bei $FF Versuchen die neue Probe
  die kleinste Distanz zu allen anderen hat. }

function tModel.IsoSubset(
  fxSmp: Tn2Sgl; //Vorbild Feature-Liste
  iSub: integer): //Anzahl Subsets (ohne Rückweisung)
  Tn2Sgl; //Feature-Liste-Subset
const
  cMax: single = MaxInt;
  cMtl = 'tMIS: Amount of samples must be larger than amount of classes!';
  cSmp = 'tMIS: Amount of samples must be larger than 1!';
  cSub = 'tMIS: Amount of classes must not be samller than 2!';
var
  faDst: TnSgl=nil; //kleinste Distanzen
  fSed: single; //quadrierte Distanz
  iCnt: integer=0; //Zelltypen erfasst
  iMin: integer=1; //Index mit kleinster Distanz
  B,R,S: integer;
begin
  Result:=nil;
  if length(fxSmp)<2 then Tools.ErrorOut(cSmp);
  if iSub<1 then Tools.ErrorOut(cSub);
  if iSub>length(fxSmp) then Tools.ErrorOut(cMtl);
  Result:=Tools.Init2Single(succ(iSub),length(fxSmp[0]),0); //Stichproben
  faDst:=Tools.InitSingle(length(Result),0);
  RandSeed:=cfRds; //Reihe zurückstellen
  for R:=1 to high(Result) do
    Result[R]:=copy(fxSmp[succ(random(high(fxSmp)))],0,length(fxSmp[0])); //Initialisierung
  repeat
    Result[0]:=copy(fxSmp[succ(random(high(fxSmp)))],0,length(fxSmp[0])); //neue Stichprobe
    FillDWord(faDst[0],length(faDst),dWord(cMax));
    for S:=1 to iSub do
      for R:=0 to pred(S) do
      begin
        fSed:=0;
        for B:=1 to high(fxSmp[0]) do
          if not(isNan(Result[R,B]) or isNan(Result[S,B])) then
          begin
            fSed+=sqr(Result[R,B]-Result[S,B]);
            faDst[R]:=min(fSed,faDst[R]); //kleinste Distanz zu beliebigem Nachbarn
            faDst[S]:=min(fSed,faDst[S]);
          end;
      end;
    for R:=0 to iSub do
      if faDst[R]<faDst[iMin] then
        iMin:=R; //Index kleinste Distanz
    if iMin>0 then
    begin
      Result[iMin]:=copy(Result[0],0,length(Result[0])); //Stichprobe übernehmen
      iCnt:=0;
    end
    else inc(iCnt); //Fehlversuche zählen
  until iCnt>$FF;
  FillDWord(Result[0,0],length(Result[0]),0); //Rückweisungs-Klasse
  Tools.BitWrite(Result,eeHme+cfMdl); //als BIT-Tabelle speichern NUR KONTROLLE
  Tools.HintOut('Model.IsoSubset: '+IntToStr(iSub));
end;

procedure tModel.RadiusDefault(fxMdl:Tn2Sgl); //Feature-Modell
{ RD initialisiert den Akzeptanz-Radius für alle Modell-Elemente. RD verwendet
  dafür für jede Klasse die kleinste Distanz zu einer der anderen Referenzen. }
var
  fSed: single=0;
  B,S,T: integer;
begin
  //length(fxMdl)>1!
  for T:=1 to high(fxMdl) do
    fxMdl[T,0]:=MaxInt; //"unendlich"
  for T:=2 to high(fxMdl) do
    for S:=1 to pred(T) do
    begin
      fSed:=0;
      for B:=1 to high(fxMdl[1]) do
        if not(isNan(fxMdl[S,B]) or isNan(fxMdl[T,B])) then
        begin
          fSed+=sqr(fxMdl[S,B]-fxMdl[T,B]);
          fxMdl[T,0]:=min(fSed,fxMdl[T,0]);
          fxMdl[S,0]:=min(fSed,fxMdl[S,0]);
        end;
    end; //for S ..
end;

function tModel.SampleThema(
  faSmp: TnSgl; //Vorbild (Probe)
  fxMdl: tn2Sgl): //spektrales Modell
  integer; //Klassen-ID
{ ST klassifiziert die Probe "faSmp" mit dem Modell "fxMdl" und gibt die
  Klassen-ID zurück. Der Distanz-Radius "fxMdl[?,0]" muss definiert sein. ST
  klassifiziert mit (quadrierten) Distanzen im Merkmals-Raum. ST ignoriert
  Klassen aus dem Modell, wenn der Distanz-Radius überschritten wird. ST gibt
  die (quadrierte) Distanz zum Modell in "faSmp[0]" zurück. }
var
  fLmt: single=MaxInt; //Schwelle
  fRds: single; //erweiterter Akzeptanz-Radius
  fSed: single; //aktuelle Distanz (quadriert)
  fzMdl: TnSgl=nil; //Zeiger auf aktuelle Definition
  B,M: integer;
begin
  Result:=0; //Vorgabe
  for M:=1 to high(fxMdl) do
  begin
    fzMdl:=fxMdl[M]; //Verweis
    fRds:=fzMdl[0]*ccScl; //Akzeptanz-Radius
    fSed:=0;
    for B:=1 to high(faSmp) do
      fSed+=sqr(fzMdl[B]-faSmp[B]); //Summe quadrierte Distanzen
    if fSed<fLmt then //beste Anpassung
      if fSed<=fRds then //Akzeptanz-Radius
      begin
        Result:=M;
        fLmt:=fSed;
      end;
  end;
  faSmp[0]:=fLmt; //quadrierte Distanz zum Modell
end;

function tModel.SampleClassify(
  fxMdl: tn2Sgl; //spektrales Modell
  fxSmp: Tn2Sgl; //Sample-Liste für Klassifikation
  iaThm: TnInt): //Klassen-IDs (muss initialisiert sein)
  integer; //Änderungen
{ SC klassifiziert die Feature-Liste "fxSmp" mit dem Modell "fxMdl", gibt das
  Ergebnis in "iaThm" zurück und zählt dabei die Klassen-Veränderungen. SC
  klassifiziert mit Distanzen im Merkmalsraum. }
var
  iThm: integer; //alte Klasse
  S: integer;
begin
  Result:=0;
{ length(iaThm)=length(fxSmp)? }
  for S:=0 to high(fxSmp) do
  begin
    iThm:=iaThm[S];
    iaThm[S]:=SampleThema(fxSmp[S],fxMdl);
    if iaThm[S]<>iThm then
      inc(Result);
  end; //for S ..
end;

{ tMA passt "fxMdl" an die Merkmale der klassifizierten Samples in "fxSmp" an.
  Dazu erzeugt tMA einen Puffer "fxTmp" und einen Zähler "iaHit", summiert alle
  klassifizierten Merkmale im Puffer und bildet den Mittelwert. Zum Schluss
  verschiebt tMA die Werte des Modells "fxMdl" in Richtung der Werte im Puffer.
  Den Grad der Anpassung steuert "ccFct". }

procedure tModel.Adjust(
  fxMdl: tn2Sgl; //Feature-Modell
  fxSmp: tn2Sgl; //Stichproben (Samples)
  iaThm: tnInt); //Klassen zu "fxSmp"
var
  fxTmp: Tn2Sgl=nil; //"Model.Template"-Kopie
  fzSmp: TnSgl=nil; //Zeiger auf aktuellen Sample
  fzMdl: TnSgl=nil; //Zeiger auf aktuelle Klasse
  iaHit: TnInt=nil; //Treffer pro Klasse
  B,M,S: integer;
begin
  //klassifizierte Merkmale summieren
  fxTmp:=Tools.Init2Single(length(fxMdl),length(fxMdl[0]),0); //für Summen
  iaHit:=Tools.InitInteger(length(fxMdl),0); //für Zähler
  for S:=0 to high(fxSmp) do
  begin
    fzSmp:=fxSmp[S];
    fzMdl:=fxTmp[iaThm[S]]; //Zeiger auf passende Klasse
    for B:=0 to high(fzMdl) do //incl. Radius!
      fzMdl[B]+=fzSmp[B]; //Summe der Merkmale
    inc(iaHit[iaThm[S]]); //Treffer pro Klasse
  end; //for S ..

  //Werte mitteln
  for M:=1 to high(fxMdl) do
    for B:=0 to high(fxMdl[0]) do //incl. Radius
      if iaHit[M]>0 then
        fxMdl[M,B]:=(fxMdl[M,B]*ccFct+fxTmp[M,B]/iaHit[M])/(ccFct+1); //gedämpft mitteln
end;

{ FM optimiert das Modell "fxMdl" für alle Stichproben in "fxSmp" durch
  Selbstorganisation. Das Modell muss (mit Beispielen) initialisiert sein. Vor
  der Optimierung setzt FM den Akzeptanz-Radius auf einen kleinen Wert. Dann
  klassifiziert FM die Stichproben "fxSmp" mit dem aktuellen Modell, passt das
  Modell an die Ergebnisse der Klassifikation an und wiederholt den Zyklus bis
  die Klassen unverändert bleiben. }

procedure tModel.FeatureModel(
  fxMdl: tn2Sgl; //spektrales Modell
  fxSmp: tn2Sgl); //Stichproben
const
  cDim = 'tMSC: [intern] Model diension does not match sample size!';
  cSmp = 'tMSC: [intern] Sample list not defined!';
var
  iaThm: TnInt=nil; //Klassen-IDs
  iMdf: integer; //Klassen-Änderungen
  iStp: integer=MaxInt; //letztes Ergebnis von "iMdf"
begin
  if length(fxSmp)<2 then Tools.ErrorOut(cSmp);
  if length(fxMdl[1])<>length(fxSmp[1]) then Tools.ErrorOut(cDim);
  iaThm:=Tools.InitInteger(length(fxSmp),0); //Klassen-IDs für Stichproben
  RadiusDefault(fxMdl); //Vorgabe
  repeat
    iMdf:=SampleClassify(fxMdl,fxSmp,iaThm);
    Adjust(fxMdl,fxSmp,iaThm);
    if iMdf*7<iStp then
    begin
      Tools.HintOut('Model.Insecure Links: '+IntToStr(iMdf));
      iStp:=iMdf;
    end;
  until iMdf<ln(high(fxMdl));
  Tools.BitWrite(fxMdl,eeHme+cfMdl); //als BIT-Tabelle speichern
  Tools.HintOut('Model.FeatureModel: '+cfMdl);
end;

procedure tModel.RadiusOpen(fxMdl:tn2Sgl);
{ RO setzt den Fang-Radius der Cluster im Modell auf "unendlich". Ein Radius<0
  würde die Klasse deaktiviren, Radius=0 identität testen. }
var
  M: integer;
begin
  if length(fxMdl)>1 then
    for M:=0 to high(fxMdl) do
      fxMdl[M,0]:=MaxInt //"unendlich"
end;

function tModel.ImageClassify(
  fxMdl: tn2Sgl; //Klassen-Vorbild
  sImg: string): //Vorbild (ENVI-Format)
  tn2Byt; //Klassifikation als Bild
{ IC klassifiziert die spektralen Attribute aller Pixel in "sImg" mit dem
  Modell "fxMdl" und gibt das Ergebnis als Byte-Matrix zurück. }
const
  cMdl = 'tMIC: No density model given!';
var
  faSmp:TnSgl=nil; //Spektral-Kombination als Array
  fxImg:Tn3Sgl=nil; //Vorbild mit allen Kanälen
  rHdr:trHdr; //Metadaten
  B,X,Y:integer;
begin
  Result:=nil;
  if fxMdl=nil then Tools.ErrorOut(cMdl);
  rHdr:=Header.Read(sImg); //Metadaten
  fxImg:=Image.Read(rHdr,sImg); //Bild mit allen Kanälen
  Result:=Tools.Init2Byte(rHdr.Lin,rHdr.Scn); //Klassen-Ergebnis
  faSmp:=Tools.InitSingle(succ(length(fxImg)),0); //Offset + Farben
  RadiusOpen(fxMdl); //alle Cluster aktivieren
  for Y:=0 to pred(rHdr.Lin) do
  begin
    for X:=0 to pred(rHdr.Scn) do
      if not IsNan(fxImg[0,Y,X]) then //nur definierte Pixel
      begin
        for B:=0 to high(fxImg) do
          faSmp[succ(B)]:=fxImg[B,Y,X]; //Pixel aus Kanälen in Array übertragen
        Result[Y,X]:=SampleThema(faSmp,fxMdl); //Klasse zuweisen
      end;
    if Y and $FF=$FF then write('.');
  end;
  write(#13); //neue Zeile
  Header.Clear(rHdr);
  Tools.HintOut('Model.ImageClassify: '+IntToStr(length(fxMdl)));
end;

{ mFC klassifiziert die scalaren Attribute aller Zonen mit dem Modell "fxMdl"
  und gibt das Ergebnis als Bild "~/.imalys/mapping" zurück. }

function tModel.FeatureClassify(fxMdl:tn2Sgl):tnInt; //Feature-Modell
const
  cMdl = 'tMFC: no feature model given!';
var
  faSmp: TnSgl=nil; //Zellmerkmale als Array
  fxAtr: Tn2Sgl=nil; //Feature-Kombination der Zelle "Z"
  B,Z: integer;
begin
  Result:=nil;
  if fxMdl=nil then Tools.ErrorOut(cMdl);
  fxAtr:=Tools.BitRead(eeHme+cfAtr); //Attribut-Tabelle lesen
  Result:=Tools.InitInteger(length(fxAtr[0]),0);
  faSmp:=Tools.InitSingle(succ(length(fxAtr)),0); //Farben + Offset
  RadiusOpen(fxMdl); //alle Cluster aktivieren
  for Z:=1 to high(fxAtr[0]) do //alle Zellen
  begin
    for B:=0 to high(fxAtr) do
      faSmp[succ(B)]:=fxAtr[B,Z]; //Merkmale einer Zelle
    Result[Z]:=SampleThema(faSmp,fxMdl); //Klasse
  end; //for Z ..
  Tools.HintOut('Model.FeatureClassify: '+IntToStr(length(Result)));
end;

function tFabric.SingleKeys(
  iIdx: integer; //Zell-ID
  iMap: integer): //Zelltypen
  TnSgl; //Model
{ fSK gibt ein Array mit der Häufigkeit der Kontakte der Zone "iIdx" zurück.
  Die Werte sind normalisiert. Sie entsprechen den Anteilen der inneren und
  äußeren Kontakte. }
var
  iPrm: integer=0;
  N: integer;
begin
  Result:=Tools.InitSingle(succ(iMap),0);
  for N:=iacDim[pred(iIdx)] to pred(iacDim[iIdx]) do
    iPrm+=iacPrm[N]; //Anzahl Kontakte
  for N:=iacDim[pred(iIdx)] to pred(iacDim[iIdx]) do
    Result[iacMap[iacNbr[N]]]+=iacPrm[N]/iPrm; //normalisierte Kontakte
end;

function tFabric.DoubleKeys(
  iIdx:integer; //Zell-ID
  iMap:integer): //Zelltypen
  TnSgl; //Zelltyp + Links + Klasse
{ fDK gibt ein Key-Array für die Zone "iIdx" zurück, das neben den Kontakten
  der zentralen Zelle die Kontakte aller Nachbarzellen enthält. Die Summe aller
  Kontakte ist normalisiert. Der Anteil aller Nachbarzellen entspricht dem
  Anteil der äußeren Kontakte. fDK speichert die Kontakte der Nachbar-Zonen
  mit Indices > Zelltypen. }
var
  fWgt:single=0;
  iExt:integer=0;
  iOfs:integer; //Offset für Peripherie
  iPrm:integer=0;
  M,N:integer;
begin
  Result:=Tools.InitSingle(succ(iMap*2),0);
  for N:=iacDim[pred(iIdx)] to pred(iacDim[iIdx]) do
    iExt+=iacPrm[N]; //alle Kontakte innere Zelle
  for N:=iacDim[pred(iIdx)] to pred(iacDim[iIdx]) do
  begin
    iPrm:=0;
    for M:=iacDim[pred(iacNbr[N])] to pred(iacDim[iacNbr[N]]) do
      iPrm+=iacPrm[M]; //alle Kontakte äußere Zelle
    fWgt:=iacPrm[N]/iExt/iPrm; //Gewicht eines Kontakts
    if iacNbr[N]<>iIdx
      then iOfs:=iMap
      else iOfs:=0;
    for M:=iacDim[pred(iacNbr[N])] to pred(iacDim[iacNbr[N]]) do
      Result[iOfs+iacMap[iacNbr[M]]]+=iacPrm[M]*fWgt; //Kontakte pro Zelltyp
  end; //for N ..
end;

function tFabric.KeySamples(
  bDbl: boolean; //Double-Layer Models verwenden
  iSmp: integer; //Anzahl Stichproben
  iTyp: integer): //Anzahl Zelltypen
  Tn2Sgl; //Stichproben
{ fKS gibt "iSmp" Key-Samples zurück, die zufällig über das Bild verteilt sind.
  Dazu wählt fKS mit einem Zufalls-Generator einzelne Pixel im Bild und gibt
  die Kontakte der entsprechenden Zone zurück. "Result[?,0]" enthält die Klasse
  der Zone. Die Verteilung der Samples ist damit geographisch normal. Die
  Topologie muss geladen sein! Große Zellen können mehr als einmal getroffen
  sein. DIE GLOBALEN VARIABLEN MÜSSEN EXISTIEREN }
const
  cMap = 'fKS: Zonal classes must be defined!';
  cTpl = 'fKS: Zonal topology must exist!';
var
  ixIdx: Tn2Int=nil; //Zellindex
  rHdr: trHdr; //Metadaten
  R,X,Y: integer;
begin
  //iSmp,iMap sind überprüft
  Result:=nil;
  if (iacDim=nil) or (iacNbr=nil) or (iacPrm=nil) then
    Tools.ErrorOut(cTpl);
  if iacMap=nil then Tools.ErrorOut(cMap);
  if bDbl
    then Result:=Tools.Init2Single(iSmp,succ(iTyp*2),0)
    else Result:=Tools.Init2Single(iSmp,succ(iTyp),0); //Stichproben
  rHdr:=Header.Read(eeHme+cfIdx); //Zellindex-Metadaten
  ixIdx:=tn2Int(Image.ReadBand(0,rHdr,eeHme+cfIdx)); //Zellindex
  RandSeed:=cfRds; //Reihe zurückstellen
  for R:=0 to high(Result) do
  begin
    repeat
      Y:=random(rHdr.Lin);
      X:=random(rHdr.Scn)
    until ixIdx[Y,X]>0; //nur definierte Orte
    if bDbl
      then Result[R]:=DoubleKeys(ixIdx[Y,X],iTyp)
      else Result[R]:=SingleKeys(ixIdx[Y,X],iTyp);
    Result[R,0]:=iacMap[ixIdx[Y,X]]; //Zelltyp
  end;
  Header.Clear(rHdr);
end;

function tFabric.KeyModel(
  bDbl:boolean; //Extended Links
  iFbr: integer; //Anzahl Klassen
  iSmp: integer): //Anzahl Stichproben
  tn2Sgl; //Modell für Keys
{ KM erzeugt ein Modell für "iFbr" Klassen auf der Basis von Zell-Keys und
  speichert das Ergebnis als "~/.imalys/keys.bit". Dazu nimmt KM "iSmp"
  geographisch gleichmäßig verteilte Stichproben aus den Zellen, wählt aus
  ihnen "iFbr" Klassen-Vorläufer und optimiert die Klassen durch Selbst-
  Organisation. DIE ZELL-TOPOLOGIE MUSS GELADEN SEIN! }
const
  cRes = 'tFKM: More than one class must be requested!';
  cSmp = 'tFKM: At least 1000 sample points are required!';
  cTpl = 'tFKM: Cell topology required!';
var
  fxSmp: Tn2Sgl=nil; //Stichproben (Zellen)
begin
  Result:=nil;
  if iFbr<2 then Tools.ErrorOut(cRes);
  if iSmp<1000 then Tools.ErrorOut(cSmp);
  if iacDim=nil then Tools.ErrorOut(cTpl);
  //iTyp:=pred(StrToInt(Header.ReadLine('classes',eeHme+cfMap)));
  RandSeed:=cfRds;
  fxSmp:=KeySamples(bDbl,iSmp,iFbr); //Stichproben mit Zell-Keys
  Result:=Model.IsoSubset(fxSmp,iFbr); //Modell aus Stichproben initialisieren
  Model.FeatureModel(Result,fxSmp); //Modell iterativ optimieren
  Tools.BitWrite(Result,eeHme+cfKey); //als BIT-Tabelle speichern NUR KONTROLLE
end;

procedure tModel.Sync_Classify(sImg:string);
{ tMSC klassifiziert alle Bilder, die mit der Maske "eeHme+cfImp+'*.hdr'
  erfasst werden können mit dem aktuellen (spektralen) Modell und gibt sie als
  "eeHme.cfMdl+'*.hdr'" zurück. Bilder und Modell müssen existieren. }
var
  fxMdl:tn2Sgl=nil; //aktuelles Modell
  sExt: string; //Extension zum Import-Name
  slApd: tStringList=nil; //Namen aller "import*"-Dateien
  I: integer;
begin
  fxMdl:=Tools.BitRead(eeHme+cfMdl); //Modell lesen
  slApd:=Tools.FileFilter(sImg); //alle Importe
  for I:=0 to pred(slApd.Count) do
  begin
    ImageClassify(fxMdl,slApd[I]); //Bild klassifizieren
    sExt:=copy(slApd[I],succ(length(sImg)),$FF);
    Tools.EnviRename(eeHme+cfMap,eeHme+cfMap+sExt)
  end;
end;

procedure tModel._SyncModel_(
  iMdl: integer; //Anzahl Klassen
  iSmp: integer; //Anzahl Stichproben
  sImg:string); //Vorbild(er)
{ tMSM erzeugt für alle Bilder die mit "import" beginnen ein gemeinsames Modell
  mit "iMdl" spektralen Klassen, speichert das Modell als "model.bit",
  klassifiziert damit alle Bilder aus dem Training und speichert sie als
  "mapping_appendix". Für das Modell clustert SM "iSmp" Stichproben der Kanal-
  Kombinationen. Sie müssen ein Minimum an definierten Werten besitzen. Die
  Clusterung beruht auf den von Teuvo Kohonen vorgeschlagenen SOMS, ist aber
  auf eine konstante Anzahl von Klassen mit variablen Suchradien eingestellt.
  Die Klassifikation folgt dem IsoClass Prinzip. }
const
  cMdl = 'tMSM: At least two different classes must be provided!';
  cSmp = 'tMSM: Each class must be trained with at least 3 samples!';
var
  fxMdl: tn2Sgl=nil; //spektrales Modell
  fxSmp: tn2Sgl=nil; //Attribut-Stichproben
begin
  if iMdl<2 then Tools.ErrorOut(cMdl);
  if iSmp<iMdl*3 then Tools.ErrorOut(cSmp);
  fxSmp:=Sync_Samples(iSmp,sImg); //Stichproben-Liste aus allen "import*" Dateien
  fxMdl:=IsoSubset(fxSmp,iMdl); //Stichproben mit maximalen Distanzen
  FeatureModel(fxMdl,fxSmp); //Selbst-Organisation der Merkmale
  Sync_Classify(sImg); //alle "input"s klassifizieren
end;

function tModel.FeatureDist:tn2Sgl; //Distanzen-Matrix
{ tMFD bestimmt die (spektrale) Distanz zwischen allen Klassen-Kombinationen im
  aktuellen Modell "fxMdl" und gibt das Ergebnis als Matrix zurück. Nicht
  definierte Kombinationen sind auf Null gesetzt. }
var
  fRes: single; //Zwischenergebnis
  fxMdl: tn2Sgl=nil; //aktuelles Modell
  I,N,M: integer;
begin
  Result:=nil;
  fxMdl:=Tools.BitRead(eeHme+cfMdl); //aktuelles Modell lesen
  Result:=Tools.Init2Single(length(fxMdl),length(fxMdl),0);
  for M:=2 to high(fxMdl) do
    for N:=1 to pred(M) do
    begin
      fRes:=0; //Vorgabe
      for I:=1 to high(fxMdl[0]) do //alle Merkmale
        fRes+=sqr(fxMdl[M,I]-fxMdl[N,I]); //Summe Quadrate
      Result[N,M]:=sqrt(fRes); //Hauptkomponente
      Result[M,N]:=Result[N,M]; //symmetrisch füllen
    end;
end;

function tModel.Sync_Samples(
  iSmp:integer; //Stichproben für alle Bilder
  sImg:string): //Vorbild
  tn2Sgl; //Stichproben als Tabelle
{ tMSS nimmt Stichproben aus allen Bildern die mit "import" beginnen und
  speichert sie in einer gemeinsamen Liste. tMSS unterstellt, dass jeder Kanal
  einer Woche entspricht und einzelne Wochen fehlen können. tMSS läd deswegen
  die Kanäle zunächst wie sie gespeichert sind und verschiebt die Einträge erst
  in den Stichproben auf die richtige Position. Fehlende Einträge werden auf
  NaN gelegt.
  ==> Der Klassifikator muss NaN ignorieren. }
var
  fxSmp: tn2Sgl=nil; //Stichproben aus aktueller Datei
  iOfs: integer; //Offset Stichproben
  slApd: tStringList=nil; //Appendices
  I,K: integer;
begin
  Result:=nil;
  slApd:=Tools.FileFilter(sImg); //alle "Import"-Dateien
  iSmp:=iSmp div slApd.Count; //Stichproben pro Datei
  for I:=0 to pred(slApd.Count) do
  begin
    fxSmp:=ImageSamples(iSmp,slApd[I]); //Sample für Bild "I"
    iOfs:=length(Result);
    SetLength(Result,length(Result)+length(fxSmp),length(fxSmp[0])); //Ergebnis vergrößern
    for K:=0 to high(fxSmp) do
      Result[iOfs+K]:=fxSmp[K]; //Adressen übertragen
  end;
  Tools.BitWrite(Result,eeHme+cfSmp+cfBit); //als BIT-Tabelle speichern NUR KONTROLLE
  writeln('Imalys [',TimeToStr(Time),'] get period image random samples');
  Tools.HintOut('Model.SyncSamples: '+IntToStr(iSmp));
end;

{ rMV gibt den Mittelwert aller Kanäle aus "fxImg" zurück. rMV überprüft NoData
  in jedem Kanal. }

function tReduce.MeanValue(fxImg:tn3Sgl):tn2Sgl; //Vorbild: Mittelwert
var
  fRes:single=0; //Summe Werte in allen Kanälen
  iCnt:integer; //Anzahl gültiger Kanäle
  B,X,Y: integer;
begin
  Result:=Tools.Init2Single(length(fxImg[0]),length(fxImg[0,0]),dWord(NaN)); //Vorgabe = NoData
  for Y:=0 to high(fxImg[0]) do
    for X:=0 to high(fxImg[0,0]) do
    begin
      fRes:=0; iCnt:=0;
      for B:=0 to high(fxImg) do
      begin
        if IsNan(fxImg[B,Y,X]) then continue;
        fRes+=fxImg[B,Y,X]; //Summe
        inc(iCnt) //Anzahl Summanden
      end;
      if iCnt>0
        then Result[Y,X]:=fRes/iCnt //Mittelwert
        else Result[Y,X]:=NaN;
    end;
end;

{ rVc bestimmt die Varianz aller Layer in "fxImg" für einzelne Pixel und gibt
  sie als Bild zurück. rVc überprüft jeden Eingangs-Kanal auf NoData, so dass
  auch lückige Bilder verarbeitet werden können. }
{ Varianz = (∑x²-(∑x)²/n)/(n-1) }

function tReduce.Variance(fxImg:tn3Sgl):tn2Sgl; //Vorbild: Varianz
var
  fSqr:single; //Summe Werte-Quadrate (x²)
  fSum:single; //Summe Werte (∑x)
  iCnt:integer; //Anzahl gültiger Kanäle (n)
  B,X,Y: integer;
begin
  //mindestens 3 Kanäle?
  Result:=Tools.Init2Single(length(fxImg[0]),length(fxImg[0,0]),dWord(NaN)); //Vorgabe = NoData
  for Y:=0 to high(fxImg[0]) do
    for X:=0 to high(fxImg[0,0]) do
    begin
      fSqr:=0; fSum:=0; iCnt:=0;
      for B:=0 to high(fxImg) do
      begin
        if IsNan(fxImg[B,Y,X]) then continue;
        fSum+=fxImg[B,Y,X];
        fSqr+=sqr(fxImg[B,Y,X]);
        inc(iCnt) //Anzahl gültige Schritte
      end;
      if iCnt>1
        then Result[Y,X]:=(fSqr-sqr(fSum)/iCnt)/pred(iCnt)
        else Result[Y,X]:=NaN;
    end;
end;

function tReduce.Regression(fxImg:tn3Sgl):tn2Sgl; //Vorbild: Mittelwert
{ rRg bestimmt die Regression aller Kanäle aus "fxImg" für einzelne Pixel und
  gibt sie als Bild zurück. rRg unterstellt, dass alle Kanäle eine gleichmäßige
  Zeitreihe bilden. rRg prüft jeden Kanal auf NoData, so dass auch lückige
  Bilder verarbeitet werden können. }
{ Regression = (∑xy-∑x∑y/n) / (∑y²-(∑y)²/n); x=Zeit, y=Wert }
var
  fDvs:single; //Dividend in Regressionsgleichung ← Nulldivision!
  fPrd:single; //Produkt aus Zeit und Wert (∑xy)
  fSqr:single; //Summe Werte-Quadrate (∑y²)
  fSum:single; //Summe Werte (∑y)
  fTms:single; //Summe Zeitachse (∑x)
  fVal:single; //aktueller Wert
  iCnt:integer; //Anzahl gültiger Kanäle (n)
  B,X,Y: integer;
begin
  //mindestens 3 Kanäle?
  Result:=Tools.Init2Single(length(fxImg[0]),length(fxImg[0,0]),dWord(NaN)); //Vorgabe = NoData
  for Y:=0 to high(fxImg[0]) do
    for X:=0 to high(fxImg[0,0]) do
    begin
      fPrd:=0; fSqr:=0; fSum:=0; fTms:=0; fVal:=0; iCnt:=0;
      for B:=0 to high(fxImg) do
      begin
        fVal:=fxImg[B,Y,X];
        if IsNan(fVal) then continue;
        fTms+=succ(B); //∑x
        fSum+=fVal; //∑y
        fPrd+=succ(B)*fVal; //∑xy
        fSqr+=sqr(fVal); //∑y²
        inc(iCnt) //Anzahl gültige Schritte
      end;
      if iCnt>0
        then fDvs:=fSqr-sqr(fSum)/iCnt
        else fDvs:=0;
      if fDvs<>0
        then Result[Y,X]:=(fPrd-fTms*fSum/iCnt)/fDvs
        else Result[Y,X]:=NaN;
    end;
end;

function tLimits._MaskLimit_(
  fMin:single; //Schwelle = kleinster zulässiger Wert
  fxBnd:tn2Sgl): //Vorbild
  tn2Byt; //Maske [0,1]
{ lML erzeugt mit der Schwelle "fMin" eine Maske ([0,1]-Kanal) aus einem
  scalaren Bild. }
var
  X,Y:integer;
begin
  Result:=Tools.Init2Byte(length(fxBnd),length(fxBnd[0])); //Maske
  for Y:=0 to high(fxBnd) do
    for X:=0 to high(fxBnd[0]) do
      if not isNan(fxBnd[Y,X]) then
        Result[Y,X]:=byte(fxBnd[Y,X]>=fMin); //Schwelle anwenden
end;

{ lE erzeugt Masken aus einem Kanal und speichert sie unter dem Namen des
  Befehls im Imalys-Verzeichnis. "sCmd" steuert die verwendete Methode. }

procedure tLimits._Execute_(
  fMin:single; //kleinster zulässiger Wert
  sCmd:string; //Befehl
  sImg:string); //Vorbild für Header
const
  cCmd = 'lE: Command not appropriate to call "limits": ';
var
  fxBnd:tn2Sgl; //Kanal
  ixMsk:tn2Byt=nil; //Ergebnis = Maske
  rHdr:trHdr; //gemeinsame Metadaten
begin
  //Result:=eeHme+cfMap;
  rHdr:=Header.Read(sImg); //gemeinsame Metadaten
  fxBnd:=Image.ReadBand(0,rHdr,sImg); //Kanal lesen
  if sCmd=cfLmt then ixMsk:=_MaskLimit_(fMin,fxBnd) else //Maske für Werte >= Null
    Tools.ErrorOut(cCmd+sCmd);
  Header.WriteThema(2,rHdr,'',eeHme+sCmd); //Maske als Klassen-Bild speichern
  Image.WriteThema(ixMsk,eeHme+sCmd); //Maske mit Klassen-Attribut
  Header.Clear(rHdr);
  Tools.HintOut('Limits.Execute: '+sCmd);
end;

function tReduce._SentinelQuality_(
  rFrm:trFrm; //Rahmen oder Vorgabe
  sImg:string; //Vorbild
  sMsk:string): //Sentinel-2 Maske im xml-Format
  tn2Byt;
begin
  Result:=nil; //leeren
  Gdal.Import(1,1,1,rFrm,sImg); //Import ohne Veränderung, Beschnitt
  Gdal.Rasterize(0,'OPAQUE',sImg,sMsk); //Polygone mit Null einbrennen
  Result:=Image.SkipMask(eeHme+cfMsk); //Maske aus Bild, Null für alle gültigen Werte
end;

function tReduce._LeafAreaIndex_(
  fxImg:tn3Sgl; //Vorbild
  iNir,iRed:integer): //Parameter
  tn2Sgl; //Vorbild: Vegetationsindex
{ rLA gibt eine Näherung für den Leaf Area Index als Kanal zurück. "iRed" und
  "iNir" müssen die Wellenländen von Rot und nahem Infrarot bezeichnen. Formel
  nach Yao et.al, 2017 }
const
  cDim = 'tFV: Vegetation index calculation needs two bands!';
var
  X,Y: integer;
begin
  if length(fxImg)<2 then Tools.ErrorOut(cDim);
  Result:=Tools.Init2Single(length(fxImg[0]),length(fxImg[0,0]),dWord(NaN)); //Vorgabe = NoData
  for Y:=0 to high(fxImg[0]) do
    for X:=0 to high(fxImg[0,0]) do
      if not IsNan(fxImg[0,Y,X]) then
        Result[Y,X]:=exp((fxImg[iNir,Y,X]-fxImg[iRed,Y,X])/(fxImg[iNir,Y,X]+
          fxImg[iRed,Y,X])*0.08); //LAI-Näherung
end;

function tReduce._LeafAreaIndex(
  fxImg:tn3Sgl; //Vorbild
  iNir,iRed:integer): //Parameter
  tn2Sgl; //Vorbild: Vegetationsindex
{ rLA gibt eine Näherung für den Leaf Area Index als Kanal zurück. "iRed" und
  "iNir" müssen die Wellenländen von Rot und nahem Infrarot bezeichnen
  (vgl. Liu_2012 "LAI") }
//fEvi:=2.5 × (NIR − RED) / (1 + NIR + 6 × RED − 7.5 × GRN)
const
  cDim = 'tFV: Vegetation index calculation needs two bands!';
var
  fEvi:single;
  X,Y:integer;
begin
  if length(fxImg)<2 then Tools.ErrorOut(cDim);
  Result:=Tools.Init2Single(length(fxImg[0]),length(fxImg[0,0]),dWord(NaN)); //Vorgabe = NoData
  for Y:=0 to high(fxImg[0]) do
    for X:=0 to high(fxImg[0,0]) do
      if not IsNan(fxImg[0,Y,X]) then
      begin
        fEvi:=2.5*(fxImg[iNir,Y,X]-fxImg[iRed,Y,X])/
          (fxImg[iNir,Y,X]+2.4*fxImg[iRed,Y,X]+1.0);
        Result[Y,X]:=-(1/0.273)*ln(1.102*(1.0-0.910*fEvi));
      end;
end;

function tReduce.Principal(fxImg:tn3Sgl):tn2Sgl; //Vorbild: Erste Hauptkomponente
{ tFP bestimmt die erste Hauptkomponente im Stack "fxImg" und gibt das Ergebnis
  als Kanal zurück. tFP verschiebt in jedem Kanal das Werte-Minimum auf Null,
  so dass alle Komponenten vollständig im positiven Raum liegen.
  ==> DIE NODATA-MASKE IM ERSTEN KANAL MUSS FÜR ALLE KANÄLE GELTEN }
const
  cDim = 'tFP: A principal component needs more than one band';
var
  //fMin:single; //kleinster Wert in allen Kanälen
  fSed: single=0; //aktuelles Ergebnis
  B,X,Y: integer;
begin
  if (length(fxImg)<2) or (length(fxImg[0,0])<1) then
    Tools.ErrorOut(cDim);
  Result:=Tools.Init2Single(length(fxImg[0]),length(fxImg[0,0]),dWord(NaN)); //Vorgabe = NoData
  for Y:=0 to high(fxImg[0]) do
    for X:=0 to high(fxImg[0,0]) do
    begin
      fSed:=0; //Vorgabe
      for B:=0 to high(fxImg) do //alle Kanäle
        if not IsNan(fxImg[B,Y,X]) then
          fSed+=sqr(fxImg[B,Y,X]);
      Result[Y,X]:=sqrt(fSed); //Länge im n-Raum
    end;
end;

function tReduce.Distance(fxImg:tn3Sgl):tn2Sgl; //Vorbild, Distanz
{ rDt gibt die euklidische Distanz im n-dimensionalen Merkmalsraum zwischen
  zwei multispektralen Bildern zurück. Der Import muss GENAU zwei Szenen
  enthalten. rDt summiert für jeden Kanal die quadrierte Differenz zwischen den
  beiden Bildern und zieht am Ende die Wurzel. }
const
  cBnd = 'fSD: Import images must have equal number of bands';
var
  X,Y:integer;
begin
  if length(fxImg)<>2 then Tools.ErrorOut(cBnd);
  Result:=Tools.Init2Single(length(fxImg[0]),length(fxImg[0,0]),dWord(NaN));
  for Y:=0 to high(Result) do
    for X:=0 to high(Result[0]) do
    begin
      if isNan(fxImg[0,Y,X]) then continue; //erstes Bild
      if isNan(fxImg[1,Y,X]) then continue; //zweites Bild
      Result[Y,X]:=fxImg[0,Y,X]-fxImg[1,Y,X]; //Differenz
    end;
end;

{ rOy überlagert alle definierten Pixel im Vorbild "fxImg" in einem Kanal. }

function tReduce.Overlay(fxImg:tn3Sgl):tn2Sgl; //Vorbilder: Mischung
var
  B,X,Y: integer;
begin
  Result:=Tools.Init2Single(length(fxImg[0]),length(fxImg[0,0]),dWord(NaN)); //Vorgabe = NoData
  for Y:=0 to high(fxImg[0]) do
    for X:=0 to high(fxImg[0,0]) do
      for B:=0 to high(fxImg) do
        if not isNan(fxImg[B,Y,X]) then
          Result[Y,X]:=fxImg[B,Y,X]; //definierte Werte überlagern
end;

procedure tReduce.QuickSort(
  faDev:tnSgl; //unsortiertes Array
  iDim:integer); //gültige Stellen im Array
{ rQS sortiert das Array "fxDev" aufsteigend. Dazu vertauscht rQS Werte, bis
  alle Vergliche passen. rQS verwendet zu Beginn große Abstände zwischen den
  Positionen im Array und reduziert sie schrittweise. }
var
  fTmp:single; //Zwischenlager
  iFix:integer; //Erfolge
  iStp:integer; //Distanz zwischen Positionen
  B:integer;
begin
  if iDim<2 then exit; //nichts zu sortieren
  iStp:=round(iDim/2); //erster Vergleich = halbmaximale Distanz
  repeat
    iFix:=0; //Vorgabe
    for B:=iStp to pred(iDim) do
      if faDev[B]>faDev[B-iStp] then //große Werte nach vorne
      begin
        fTmp:=faDev[B-iStp];
        faDev[B-iStp]:=faDev[B];
        faDev[B]:=fTmp;
      end
      else inc(iFix);
      if iStp>1 then iStp:=round(iStp/2) //Distanz halbieren
  until iFix=pred(iDim); //alle Vergleiche richtig
end;

function tReduce.Median(fxImg:tn3Sgl):tn2Sgl; //Vorbild: Median
{ rMn bildet den Median aus allen übergebenen Kanälen. }
{ Dazu kopiert rMn alle Werte eines Pixels nach "fxDev", sortiert "fxDev" mit
  "QuickSort" und übernimmt den Wert in der Mitte der gültigen Einträge in
  "fxDev". rMn kopiert NoData Werte in den Bilddaten nicht nach "faDev" sondern
  reduziert mit "iDim" die gültigen Stellen in "faDev". }
const
  cDim = 'rMn: Less than three bands provided for median calculation';
var
  faDev:tnSgl=nil; //ein Pixel aus allen Kanälen
  iDim:integer; //Anzahl Kanäle
  B,X,Y: integer;
begin
  if length(fxImg)<3 then Tools.ErrorOut(cDim);
  Result:=Tools.Init2Single(length(fxImg[0]),length(fxImg[0,0]),dWord(NaN)); //Vorgabe = NoData
  SetLength(faDev,length(fxImg)); //alle Kanäle
  for Y:=0 to high(fxImg[0]) do
  begin
    for X:=0 to high(fxImg[0,0]) do
    begin
      iDim:=0;
      for B:=0 to high(faDev) do
        if not isNan(fxImg[B,Y,X]) then
        begin
          faDev[iDim]:=fxImg[B,Y,X]; //Pixel-Stack
          inc(iDim)
        end;
      if iDim>2 then
      begin
        QuickSort(faDev,iDim); //ordnen
        Result[Y,X]:=faDev[trunc(iDim/2)] //median
      end;
    end;
    if Y and $FF=0 then write('.');
  end;
end;

function tFabric.ClassifyKeys(
  bDbl:boolean; //extended Links
  fxKey:tn2Sgl): //Key-Modell
  TnInt; //Klassen-Attribut (Key-basiert)
{ fCK klassifiziert alle Zonen mit dem Modell "fxKey" und gibt das Ergebnis als
  Array mit Klassen-IDs zurück. fCK klassifiziert mit dem Anteil der Kontakte
  zwischen den Zonen. fCK öffnet den Radius der Neurone um lückenlos zu
  klassifizieren. Mit "bDbl=True" erzeugt fCK zweischichtige Links. }
const
  cTpl = 'tFFC: Cell topology required! (intern)';
var
  faSmp: TnSgl=nil; //Feature-Kombination für Zone "Z"
  iMap: integer; //Anzahl Zonen-Klassen
  Z: integer;
begin
  Result:=nil;
  if iacDim=nil then Tools.ErrorOut(cTpl);
  iMap:=high(fxKey[0]); //Anzahl Zonen-Klassen
  Model.RadiusOpen(fxKey);
  Result:=Tools.InitInteger(length(iacDim),0); //Klassen-Attribut
  for Z:=1 to high(iacDim) do //alle Zonen
  begin
    if bDbl
      then faSmp:=Fabric.DoubleKeys(Z,iMap)
      else faSmp:=Fabric.SingleKeys(Z,iMap);
    Result[Z]:=Model.SampleThema(faSmp,fxKey);
  end; //for Z ..
end;

{ mZm klassifiziert Zonen-Attribute und gibt das Ergebnis als Array zurück.
  mZm nimmt "iSmp" Stichproben aus dem Zonen-Bild "index", speichert ihre
  Attribute in "fxSmp" und leitet aus den Stichproben "iFtr" Klassen ab. mZm
  wählt "iFtr" möglichst unterschiedliche Attribut-Kombinationen aus den
  Stichproben und optimiert sie durch Selbstorganisation. Am Ende klassifiziert
  mZm alle Zonen mit den endgültigen Klassen. }

function tModel.ZonalMap(
  iFbr:integer; //Anzahl Klassen
  iSmp:integer): //Anzahl Stichproben
  tnInt; //Klassen-Attribut
var
  fxMdl:tn2Sgl=nil; //spektrales Modell
  fxSmp:tn2Sgl=nil; //Attribut-Stichproben
begin
  //iFbr,iSmp sind überprüft
  Result:=nil; //Vorgabe
  Build.CheckZones('mZm'); //Zonen definiert?
  fxSmp:=AttributeSamples(iSmp); //Stichproben aus Attributen
  fxMdl:=IsoSubset(fxSmp,iFbr); //Stichproben mit maximalen Distanzen
  FeatureModel(fxMdl,fxSmp); //Selbst-Organisation der Merkmale
  Result:=FeatureClassify(fxMdl); //Klassen-Attribut
end;

{ fFx klassifiziert alle Zonen mit der Häufigkeit der Zonen-Kontakte. Dazu
  benötigtfFx eine Zonen-Klassifikation "iacMap" und speichert sie global. fFx
  läd die Topologie (iacDim, iacNbr, iacPrm) ebenfalls global. fFx nimmt "iSmp"
  gleichmäßig verteilte Stichproben aus dem Bild und erzeugt damit ein Modell
  "fxKey" für die Häufigkeit der Zonen-Kontakte. Mit dem Modell klassifiziert
  fFx das Bild (ixThm). Der Klassifikator verwendet Kohonen-Neurone. fFx
  speichert das Ergebnis mit Zufallsfarben als thematisches Bild "fabric". }

procedure tFabric.xFabric(
  bDbl:boolean; //Extended Links
  iFbr:integer; //Anzahl Key-Klassen
  iSmp:integer); //Anzahl Stichproben
const
  cFbr = 'fFc: Number of fabric classes must exeed 2!';
  cFex = 'fFc: Zones nt found: ';
  cSmp = 'fFc: Number of fabric samples must exeed 1000!';
var
  fxKey: tn2Sgl=nil; //Keys für Zelltyp-Verknüpfungen
  iaFbr: tnInt=nil; //Klassen-Attribut
  ixThm: tn2Byt=nil; //Cell-Objekt-Klassen als Bild
  rHdr:trHdr; //Metadaten aus Zellindex
begin
  if iFbr<2 then Tools.ErrorOut(cFbr);
  if iSmp<1000 then Tools.ErrorOut(cSmp);
  if not FileExists(eeHme+cfIdx) then Tools.ErrorOut(cFex+eeHme+cfIdx);
  iacMap:=Model.ZonalMap(iFbr,iSmp); //Klassen-Attribut
  iacDim:=tnInt(Tools.BitExtract(0,eeHme+cfTpl));
  iacNbr:=tnInt(Tools.BitExtract(1,eeHme+cfTpl));
  iacPrm:=tnInt(Tools.BitExtract(2,eeHme+cfTpl));
  fxKey:=KeyModel(bDbl,iFbr,iSmp); //Key-Modell erzeugen
  iaFbr:=ClassifyKeys(bDbl,fxKey); //Klassen (Key-basiert)
  ixThm:=Build.ThemaImage(iaFbr); //Bild aus Klassen-Attribut
  rHdr:=Header.Read(eeHme+cfIdx); //Geometrie
  Header.WriteThema(iFbr,rHdr,'',eeHme+cfMap); //Klassen-Header
  Image.WriteThema(ixThm,eeHme+cfMap); //Klassen als Bild
  SetLength(iacDim,0);
  SetLength(iacMap,0);
  SetLength(iacNbr,0);
  SetLength(iacPrm,0);
  Header.Clear(rHdr);
end;

{ mSM klassifiziert alle Pixel in "sImg" durch Selbstorganisation und gibt das
  Ergebnis als Klassen-Layer zurück. mSM wählt "iSmp" gleichmäßig verteilte
  Stichproben (Pixel) aus dem Bild (fxSmp) und bildet sie auf "iFtr" Klassen
  ab. Die Klassen sind zunächst möglichst verschieden. mSm optimiert sie durch
  Selbstorganisation. Mit dem Ergebnis klassifiziert mSM das Bild "sImg". }

function tModel.SpectralMap(
  iFtr:integer; //Anzahl Klassen
  iSmp:integer; //Anzahl Stichproben
  sImg:string): //Vorbild: Zellindex oder normales Bild
  tn2Byt; //Klassen-Layer
var
  fxMdl:tn2Sgl=nil; //spektrales Modell
  fxSmp:tn2Sgl=nil; //Attribut-Stichproben
begin
  Result:=nil; //Vorgabe = leer
  Image.AlphaMask(sImg); //NoData-Maske auf alle Kanäle ausdehnen
  fxSmp:=ImageSamples(iSmp,sImg); //Stichproben aus Bilddaten
  fxMdl:=IsoSubset(fxSmp,iFtr); //Stichproben mit maximalen Distanzen
  FeatureModel(fxMdl,fxSmp); //Selbst-Organisation der Merkmale
  Result:=ImageClassify(fxMdl,sImg); //Cluster-Layer
end;

{ TODO: [MODEL.xModel] Zonen-Attribute auf NoData überprüfen. "AlphaMask" hat
        die spektralen Merkmale vereinheitlicht. neue Attribute können anders
        aussehen }

{ mMl erzeugt ein Klassen-Modell und klassifiziert Bilddaten oder Zonen. Wird
  mit "sImg" der Zonen-Index übergeben, klassifiziert mMl Zonen-Attribute. Die
  Attribute müssen existieren. In allen anderen Fällen klassifiziert mMl Pixel.
  In beiden Fällen schreibt mEx das Ergebnis in einen Klassen-Layer "mapping". }

procedure tModel.xModel(
  iFtr:integer; //Anzahl Klassen
  iSmp:integer; //Anzahl Stichproben
  sImg:string); //Vorbild + Schalter für Pixel-Klassen
const
  cFex = 'mMl: Image not found: ';
var
  ixMap:tn2Byt=nil; //Klassen-Bild
  rHdr:trHdr; //Metadaten
begin
  if not FileExists(sImg) then Tools.ErrorOut(cFex+sImg);
  if sImg=eehme+cfIdx //Zonen-Index
    then ixMap:=Build.ThemaImage(ZonalMap(iFtr,iSmp)) //Klassen-Layer aus Zonen
    else ixMap:=SpectralMap(iFtr,iSmp,sImg); //Klassen-Layer aus Pixelbild
  Rank.SortByte(iFtr,ixMap); //Klassen-ID nach Fläche
  Image.WriteThema(ixMap,eeHme+cfMap); //
  rHdr:=Header.Read(sImg); //Vorbild (Bild oder Index)
  Header.WriteThema(iFtr,rHdr,'',eeHme+cfMap); //umwadeln und speichern
  Header.Clear(rHdr);
  Tools.HintOut('Model.Execute: '+cfMap)
end;

procedure tReduce.IndexSort(
  iaIdx:tnInt; //Indices der übergebenen Werte
  faVal:tnSgl); //Werte, unsortiert → sortiert
{ rIS sortiert das Array "faVal" absteigend. Dazu vertauscht rIS Werte und die
  mit dem Wert verknüpften Indices in "iaIdx" bis alle Vergliche passen. rIS
  verwendet zu Beginn große Abstände zwischen den Positionen im Array und
  reduziert sie schrittweise.
  ==> Werte statt Zeiger zu sortieren kann schneller sein, wenn die Werte klein
      und die Vorbereitung der Werte aufwändig ist.
  ==> vgl. Reduce.QuickSort }
var
  fVal:single; //Zwischenlager
  iFix:integer; //Erfolge
  iStp:integer; //Distanz zwischen Positionen
  iIdx:integer; //Zwischenlager
  B:integer;
begin
  //length(iaIdx)=length(faVal)?
  if length(iaIdx)<2 then exit; //nichts zu sortieren
  iStp:=round(length(iaIdx)/2); //erster Vergleich = halbmaximale Distanz
  repeat
    iFix:=0; //Vorgabe
    for B:=iStp to high(iaIdx) do
      if faVal[B]>faVal[B-iStp] then //große Werte nach vorne
      begin
        iIdx:=iaIdx[B-iStp];
        fVal:=faVal[B-iStp];
        iaIdx[B-iStp]:=iaIdx[B];
        faVal[B-iStp]:=faVal[B];
        iaIdx[B]:=iIdx;
        faVal[B]:=fVal;
      end
      else inc(iFix);
      if iStp>1 then iStp:=round(iStp/2) //Distanz halbieren
  until iFix=high(iaIdx); //alle Vergleiche richtig
end;

{ rV gibt den Near Infrared Vegetation Index als Kanal zurück. "iRed" und
  "iNir" müssen die Wellenländen von Rot und nahem Infrarot bezeichnen. }

function tReduce.Vegetation(
  fxImg:tn3Sgl; //Vorbild
  iNir,iRed:integer; //Parameter
  iTyp:integer): //Index-ID
  tn2Sgl; //Vorbild: Vegetationsindex
const
  cDim = 'rVn: Vegetation index calculation needs two bands!';
  cTyp = 'rVn: Undefined ID for vegetation index"';
var
  X,Y: integer;
begin
  if length(fxImg)<2 then Tools.ErrorOut(cDim);
  if (iTyp<0) or (iTyp>2) then Tools.ErrorOut(cTyp);
  Result:=Tools.Init2Single(length(fxImg[0]),length(fxImg[0,0]),dWord(NaN)); //Vorgabe = NoData
  for Y:=0 to high(fxImg[0]) do
    for X:=0 to high(fxImg[0,0]) do
      if not IsNan(fxImg[0,Y,X]) then
        case iTyp of
          0: Result[Y,X]:=(fxImg[iNir,Y,X]-fxImg[iRed,Y,X])/(fxImg[iNir,Y,X]+
             fxImg[iRed,Y,X])*fxImg[iNir,Y,X]; //NIRv Vegetattionsindex
          1: Result[Y,X]:=(fxImg[iNir,Y,X]-fxImg[iRed,Y,X])/(fxImg[iNir,Y,X]+
             fxImg[iRed,Y,X]); //NDVI Vegetationsindex
          2: Result[Y,X]:=2.5*(fxImg[iNir,Y,X]-fxImg[iRed,Y,X])/
             (fxImg[iNir,Y,X]+2.4*fxImg[iRed,Y,X]+1.0); //EVI Vegetationsindex
        end;
end;

//EncodeDate()tDateTime formatiert
//CompareDate() vergleicht
//DaysBetween() vergleicht

function tReduce.ImageDate(sImg:string):tDateTime;
begin
  Result:=EncodeDate(
    StrToInt(copy(sImg,1,4)),
    StrToInt(copy(sImg,5,2)),
    StrToInt(copy(sImg,7,2)));
end;

{ rCD sucht in "slImg" nach Bildern mit gleichem Datum und verschiebt die
  Dateinamen in das Ergebnis. rCD gibt immer nur EIN Datum mit mehr als einem
  Bild zurück und kann widerholt aufgerufen werden bis alle Bilder von
  verschiedenen Tagen stammen. Dazu rCD sucht rCD nach Bildern die vom gleichen
  Tag stammen wie das erste und markiert sie durch eine Dummy-Adresse. Da mehr
  als zwei Bilder vom gleichen Tag stammen können, durchsucht lCD die gesamte
  Liste. }

function tReduce.CommonDate(
  slImg:tStringList): //WIRD REDUZIERT!
  tStringList; //passende Bilder ODER leer
var
  bSkp:boolean=False; //Suche beendet
  rDat:tDateTime; //Datum Vorbild
  I,K:integer;
begin
  Result:=tStringList.Create; //leere Liste
  if slImg.Count>1 then
  repeat
    for I:=pred(slImg.Count) downto 1 do //Referenz
    begin
      rDat:=ImageDate(RightStr(slImg[I],8));
      for K:=0 to pred(I) do //Vergleich, alle Kombinationen
        if ImageDate(RightStr(slImg[K],8))=rDat then
        begin
          slImg.Objects[I]:=tObject($01); //Markierung setzten mit leerer Adresse
          slImg.Objects[K]:=tObject($01);
          bSkp:=True;
        end;
      if bSkp then break;
    end;
  until bSkp or (I=1);

  for I:=pred(slImg.Count) downto 0 do
    if slImg.Objects[I]=tObject($01) then
    begin
      Result.Add(slImg[I]); //Bilder mit gleichem Datum
      slImg.Delete(I) //markierte Bildnamen löschen
    end;
end;

{ TODO: Reduce.BestOf könnte auch dann die zwei Layer Regel anwenden, wenn
        einzelne Layer Löcher haben. Dazu müsste analog zu "faDev" ein "faQlt"
        Feld gefült werden, in das die QA-Indices ohne Lücken eingetragen
        werden. }

{ rBO übernimmt den "besten" Pixel aus einem beliebigen Stack. rBO kopiert alle
  definierten Werte eines Pixels nach "faDev". Sind mehr als 2 Werte definiert,
  sortiert rBO die Werte und übernimmt den Wert in der Mitte (Median). Bei zwei
  Werten bildet rBO den Mittelwert, wenn beide Bilder sehr wenig Fehler haben,
  andernfalls das "bessere" Bild. Ein Kanal wird unverändert übernommen. rBO
  füllt leere Bereiche mit NoData. }

function tReduce.BestOf(
  fxImg:tn3Sgl; //Vorbild
  sQap:string): //Qualitäts-Indices
  tn2Sgl; //Median
var
  bRnk:boolean=false; //
  fHig,fLow:single; //QA-Index für zweiten, ersten Layer
  faDev:tnSgl=nil; //ein Pixel aus allen Kanälen
  iDim:integer; //Anzahl Kanäle
  B,X,Y: integer;
begin
  Result:=Tools.Init2Single(length(fxImg[0]),length(fxImg[0,0]),dWord(NaN)); //Vorgabe = NoData
  SetLength(faDev,length(fxImg)); //alle Kanäle

  if WordCount(sQap,[','])=2 then //Mittelwert oder Präfenz bei zwei Layern
  begin
    fHig:=StrToFloat(ExtractWord(2,sQap,[',']));
    fLow:=StrToFloat(ExtractWord(1,sQap,[',']));
    bRnk:=abs((fHig-fLow)/(fHig+fLow))>0.5;
  end;

  for Y:=0 to high(fxImg[0]) do
  begin
    for X:=0 to high(fxImg[0,0]) do
    begin
      iDim:=0;
      for B:=0 to high(faDev) do
        if not isNan(fxImg[B,Y,X]) then
        begin
          faDev[iDim]:=fxImg[B,Y,X]; //Pixel-Stack
          inc(iDim)
        end;
      if iDim>2 then
      begin
        QuickSort(faDev,iDim); //ordnen
        Result[Y,X]:=faDev[trunc(iDim/2)] //median
      end
      else if iDim>1 then
      begin
        if bRnk=False then
          Result[Y,X]:=(faDev[0]+faDev[1])/2 //Mittelwert
        else if fHig>fLow
          then Result[Y,X]:=faDev[1]
          else Result[Y,X]:=faDev[0]
      end
      else if iDim>0 then
        Result[Y,X]:=faDev[0] //erster Wert
      else Result[Y,X]:=NaN; //nicht definiert
    end;
  end;
end;

{ rEx reduziert ein Multikanal-Bild zu einem Kanal. Der Prozess wird durch die
  Konstante "sCmd" gewählt. Für die Vegetationsindices ist zusätzlich die ID
  des Kanals für ROT und NIR notwendig. "BestOf" benötigt die Anteile klarer
  Pixel im Bild. }

function tReduce.Execute(
  fxImg:tn3Sgl; //Vorbild, >1 Kanal
  iNir,iRed:integer; //Kanal-Indices NUR für Vegetationsindex
  sCmd:string; //Reduktions-Befehl (Konstante)
  s_Xpq:string): //Qualitäts-Indices aus Header
  tn2Sgl; //Ergebnis
begin
  if sCmd=cfBst then Result:=BestOf(fxImg,s_Xpq) else //Median-Mean-Defined
  if sCmd=cfDff then Result:=Distance(fxImg) else //Euklidische Distanz
  if sCmd=cfLai then Result:=_LeafAreaIndex(fxImg,iNir,iRed) else //LAI-Näherung
  if sCmd=cfMdn then Result:=Median(fxImg) else //Median
  if sCmd=cfMea then Result:=MeanValue(fxImg) else //Mittelwert
  if sCmd=cfOvl then Result:=Overlay(fxImg) else //Überlagerung
  if sCmd=cfPca then Result:=Principal(fxImg) else //Hauptkomponente
  if sCmd=cfRgs then Result:=Regression(fxImg) else //simple Regression
  if sCmd=cfNiv then Result:=Vegetation(fxImg,iNir,iRed,0) else //NirV Index
  if sCmd=cfNvi then Result:=Vegetation(fxImg,iNir,iRed,1) else //NDVI Index
  //if sCmd=cfEvi then Result:=_Vegetation(fxImg,iNir,iRed,2) else //EVI Index
  if sCmd=cfVrc then Result:=Variance(fxImg) else //Varianz
  begin end;
end;

{ rRe reduziert alle Kanäle in "sImg" auf einen Ergebnis-Kanal und speichert
  ihn unter dem Namen des Befehls "sCmd". "iNir" und "iRed" werden nur für den
  Vegetationsindex benötigt. }

procedure tReduce.xReduce(
  iNir,iRed:integer; //Kanäle für Vegetationsindex
  sCmd:string; //Befehl
  sImg:string; //Name des Vorbilds
  sTrg:string); //Name Ergebnis ODER leer
const
  cFex = 'rRe: Image not found: ';
var
  fxRes:tn2Sgl=nil; //Ergebnis der Reduktion
  fxStk:tn3Sgl=nil; //Stack zur Reduktion
  rHdr:trHdr; //Metadaten
begin
  if not FileExists(sImg) then Tools.ErrorOut(cFex+sImg);
  rHdr:=Header.Read(sImg);
  fxStk:=Image.Read(rHdr,sImg); //Import vollständig
  fxRes:=Execute(fxStk,iNir,iRed,sCmd,rHdr.Qap); //Befehl anwenden
  if trim(sTrg)='' then sTrg:=eeHme+sCmd; //Vorgane = Name des Befehls
  Image.WriteBand(fxRes,-1,sTrg); //neue Datei aus Ergebnis
  rHdr.Qap:=FloatToStrF(_Quality(rHdr.Qap),ffFixed,7,3); //mittlere Qualität
  Header.WriteScalar(rHdr,sTrg); //Header für einen Kanal
  Tools.HintOut('Reduce.Execute :'+ExtractFileName(sTrg)); //Status
  Header.Clear(rHdr)
end;

{ rSc reduziert gestapelte multispektrale Bilder zu einem multispektralen Bild.
  Dabei reduziert rSc gleiche Kanäle aus verschiedenen Bildern mit dem Befehl
  "sCmd" zu jeweils einem Kanal und speichert die neuen Kanäle in der alten
  Reihenfolge unter dem Namen des Befehls. Alle Bilder im Stapel "sImg" müssen
  dieselben Kanäle haben. rSc liest und schreibt im ENVI-Format.
  ==> vgl. "xReduce"
  ==> rSc KANN VEG UND LAI NICHT AUSFÜHREN
  ==> DIE METADATEN VON SIMG MÜSSEN DIE KANAL-NAMEN ENTHALTEN }

procedure tReduce.xSplice(
  sCmd:string; //Prozess
  sImg:string; //Vorbild
  sTrg:string); //Ergebnis-Name ODER leer für Prozess-Name
const
  cFex = 'rSe: Image not found: ';
var
  fxRes:tn2Sgl=nil; //Ergebnis-Kanal für aktuelle Gruppe
  fxStk:tn3Sgl=nil; //Kanäle aus Import mit gleicher Gruppen-Nr
  iRes:integer=-1; //aktueller Kanal, zu Beginn "-1" für neues Bild
  rHdr:trHdr; //Metadaten
  sBnd:string=''; //Kanal-Namen, Zeilen
  B,I:integer;
begin
  if not FileExists(sImg) then Tools.ErrorOut(cFex+sImg);
  rHdr:=Header.Read(sImg);
  if trim(sTrg)='' then sTrg:=eeHme+sCmd; //Vorgabe = Prozess-Name
  if rHdr.Prd<rHdr.Stk then //nur wenn mehr als ein Bild
  begin
    SetLength(fxStk,rHdr.Stk div rHdr.Prd,1,1); //Dummy, Ein Kanal für jedes Bild
    for B:=0 to pred(rHdr.Prd) do //alle Ergebnis-Kanäle
    begin
      for I:=0 to pred(rHdr.Stk div rHdr.Prd) do //alle Vorbilder
        fxStk[I]:=Image.ReadBand(I*rHdr.Prd+B,rHdr,sImg); //Kanal "B" aus Bild "I" laden
      fxRes:=Execute(fxStk,3,2,sCmd,rHdr.Qap); //multiplen Kanal reduzieren
      if B>0 then iRes:=B; //neues Bild (-1) oder neuer Kanal (I)
      Image.WriteBand(fxRes,iRes,sTrg); //Kanal schreiben
//------------------------------------------------------------------------------
      //sBnd+=LeftStr(ExtractWord(succ(B),rHdr.aBnd,[#10]),6)+sCmd+#10; //Sensor+Kanal+Prozess
      if sCmd<>cfOvl
        then sBnd+=LeftStr(ExtractWord(succ(B),rHdr.aBnd,[#10]),6)+sCmd+#10 //Sensor+Kanal+Prozess
        else sBnd+=ExtractWord(succ(B),rHdr.aBnd,[#10])+#10; //
//------------------------------------------------------------------------------
    end;
    rHdr.Qap:=FloatToStrF(_Quality(rHdr.Qap),ffFixed,7,3); //mittlere Qualität
    Header.WriteMulti(rHdr,sBnd,sTrg); //multikanal
  end
  else Tools.CopyEnvi(sImg,sTrg); //unverändert verwenden
  Header.Clear(rHdr);
  Tools.HintOut('Reduce.Splice: '+sCmd);
end;

// bestimmt mittlere Qualität überlagerte Bilder

function tReduce._Quality(sQlt:string):single;
var
  I:integer;
begin
  Result:=0; //Vorgabe
  //if sQlt[1]='{' then delete(sQlt,1,1); //Klammern entfernen
  //if sQlt[length(sQlt)]='}' then delete(sQlt,length(sQlt),1);
  for I:=1 to WordCount(sQlt,[',']) do
    Result+=StrToFloat(ExtractWord(I,sQlt,[','])); //Summe aus Bildern
  if length(sQlt)>0
    then Result/=WordCount(sQlt,[',']) //mittlere Qualität
    else Result:=1; //Bilder notfalls verwenden
end;

{ TODO: [IMPORT] Overlay könnte auch Bilder aus benachbarten Flugpfaden
        vereinigen, wenn die Aufnahmezeitpunkte nahe beieinander liegen }

{ rOy vereinigt Teilbilder, die am gleichen Tag aufgenommen wurden und löscht
  die Vorbilder. rOy prüft sukzessive ob die Liste "slImg" Bilder mit gleichem
  Datum enthält. Wenn ja, überlagert rOy die Teilbilder, speichert sie unter
  einem neuen Namen aus Sensor und Datum (ohne Kachel-ID) und löscht die
  Teilbilder. Gleichzeitig ersetzt rOy die Namen der Teilbilder in "slImg"
  durch das vereinigte Bild. Bilder mit gleichem Datum stammen aus einem
  Flugpfad. Sie sind identisch. }

procedure tReduce.xOverlay(slImg:tStringList); //WIRD REDUZIERT!
var
  slOvl:tStringList=nil; //Bilder mit gleichem Datum
  sTmp:string=''; //Zwischenlager
  I:integer;
begin
  repeat
    try
      slOvl:=CommonDate(slImg); //verschiebt Namen von "slImg" nach "slOvl"
      if slOvl.Count>0 then
      begin
        Image.StackImages(slOvl,eeHme+cfStk); //Kacheln in gemeinsamen Rahmen
        Reduce.xSplice(cfOvl,eeHme+cfStk,''); //gleiche Kanäle überlagern
        sTmp:=ExtractFilePath(slOvl[0])+LeftStr(ExtractFileName(slOvl[0]),5)+
          RightStr(ExtractFileName(slOvl[0]),8); //neuer Name
        Tools.EnviRename(eeHme+cfOvl,sTmp); //Sensor + Datum
        for I:=0 to pred(slOvl.Count) do
          Tools.EnviDelete(slOvl[I]); //Teilbilder löschen
        slImg.Add(sTmp); //Ergebnis übernehmen
      end
      else sTmp:=''; //Schalter
    finally
      FreeAndNil(slOvl);
    end;
  until sTmp='';
end;

initialization

  Fabric:=tFabric.Create;
  Fabric.iacDim:=nil;
  Fabric.iacMap:=nil;
  Fabric.iacNbr:=nil;
  Fabric.iacPrm:=nil;

finalization

  SetLength(Fabric.iacDim,0);
  SetLength(Fabric.iacMap,0);
  SetLength(Fabric.iacNbr,0);
  SetLength(Fabric.iacPrm,0);
  Fabric.Free;

end.

{==============================================================================}

{ rOy vereinigt Teilbilder, die am gleichen Tag (gleicher Überflug) aufgenommen
  wurden und löscht die Vorbilder. rOy prüft sukzessive ob die Liste "slImg"
  Bilder mit gleichem Datum enthält. Wenn ja, überlagert rOy die Bilddaten,
  speichert sie unter einem Namen aus Sensor und Datum (ohne Kachel-ID) und
  löscht die Vorbilder. Bilder mit gleichem Datum stammen aus einer Flugspur.
  Sie sind identisch. }

procedure tReduce.xOverlay_(slImg:tStringList); //WIRD REDUZIERT!
var
  slOvl:tStringList=nil; //Bilder mit gleichem Datum
  sTmp:string=''; //Zwischenlager
  I:integer;
begin
  repeat
    try
      slOvl:=CommonDate(slImg); //verschiebt Namen von "slImg" nach "slOvl"
      if slOvl.Count>0 then
      begin
        Image.StackImages(slOvl); //Kacheln in gemeinsamen Rahmen
        Reduce.xSplice(cfOvl,eeHme+cfStk,''); //gleiche Kanäle überlagern
        sTmp:=ExtractFilePath(slOvl[0])+LeftStr(ExtractFileName(slOvl[0]),5)+
          RightStr(ExtractFileName(slOvl[0]),8); //neuer Name
        Tools.EnviRename(eeHme+cfOvl,sTmp); //Sensor + Datum
        for I:=0 to pred(slOvl.Count) do
          Tools.EnviDelete(slOvl[I]); //Teilbilder löschen
      end
      else sTmp:=''; //Schalter
    finally
      FreeAndNil(slOvl);
    end;
  until sTmp='';
end;

