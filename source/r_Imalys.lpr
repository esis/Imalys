program r_Imalys;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}
  cthreads,
  {$ENDIF}
  Classes, SysUtils, loop, format;

const
  cPrm = 'Repeat Imalys call: Commands AND parameter files must be provided!';
begin
  if ParamCount>1
    then Change._LoopImalys(ParamStr(1),ParamStr(2))
    else raise Exception.Create(cPrm);
end.

